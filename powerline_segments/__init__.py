from .cpu import (
    cpu_freq,
    cpu_usage,
)
from .sensor import (
    read_cpu_temp,
    read_fan_speed,
)
from .sys import (
    apc_ups_power_usage,
    mail_usage,
    memory_usage,
)
