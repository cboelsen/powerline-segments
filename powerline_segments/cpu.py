import math
import psutil

from dataclasses import asdict, dataclass

from .cache import cached_value
from .utils import calc_gradient


@dataclass
class ProcStat:
    user: int
    nice: int
    system: int
    idle: int
    iowait: int
    irq: int
    softirq: int
    steal: int
    guest: int
    guest_nice: int

    def __sub__(self, other):
        return self.__class__(
            self.user - other.user,
            self.nice - other.nice,
            self.system - other.system,
            self.idle - other.idle,
            self.iowait - other.iowait,
            self.irq - other.irq,
            self.softirq - other.softirq,
            self.steal - other.steal,
            self.guest - other.guest,
            self.guest_nice - other.guest_nice,
        )


class CpuUsage:

    def __init__(self, line=None):
        if line:
            stats = []
        else:
            stats = self._read()
            line = stats.pop(0)
        self.previous = self._parse(line)
        self.current = None
        self.diff = None
        self.children = [CpuUsage(l) for l in stats]

    def _read(self):
        with open("/proc/stat") as f:
            cpu_usage_output = f.readlines()
        return [l for l in cpu_usage_output if l.startswith("cpu")]

    def _parse(self, line):
        _, *figures = line.split()
        return ProcStat(*[int(f) for f in figures])

    def _update(self, line):
        self.current = self._parse(line)
        self.diff = self.current - self.previous
        self.previous = self.current

    def update(self):
        stats = self._read()
        self._update(stats.pop(0))
        for child in self.children:
            child._update(stats.pop(0))

    def idle(self):
        total = sum(asdict(self.diff).values())
        return (self.diff.idle + self.diff.iowait) * 100.0 / total

    def used(self):
        return 100.0 - self.idle()

    def individual_used(self):
        return max([c.used() for c in self.children])


usage = CpuUsage()


@cached_value(3)
def cpu_usage(pl, format="{:3.0f} [{:3.0f}]%"):
    usage.update()
    reading = usage.used()
    individual = usage.individual_used()
    gl = calc_gradient(reading, 35, 65)
    return [{
        'contents': format.format(round(reading), round(individual)),
        'gradient_level': gl,
        'highlight_groups': ['cpu_load_percent_gradient', 'cpu_load_percent'],
    }]

@cached_value(3)
def cpu_freq(pl, format="{:1.1f}GHz"):
    freq_mhz = max([p.current for p in psutil.cpu_freq(percpu=True)])
    reading = freq_mhz / 1000
    gl = calc_gradient(reading, 1.8, 4)
    return [{
        'contents': format.format(reading),
        'gradient_level': gl,
        'highlight_groups': ['cpu_load_percent_gradient', 'cpu_load_percent'],
    }]
