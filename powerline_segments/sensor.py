import subprocess

from .cache import cached_value
from .utils import calc_gradient


class Reading:

    def __init__(self, text):
        name, rest = text.split(":", 1)
        self.name = name.strip()
        value = rest.strip().split("  ")[0]
        if value.endswith("°C"):
            self.value = value[:-2]
            self.unit = "°C"
        else:
            try:
                self.value, self.unit = value.split(" ", 1)
            except ValueError:
                self.value = value
                self.unit = ""
        if self.value.startswith("+"):
            self.value = self.value[1:]
        try:
            self.value = float(self.value)
        except ValueError:
            pass


class Sensor:

    def __init__(self, text):
        lines = text.splitlines()
        self.name = lines[0].strip()
        self.adapter = lines[1].split(": ")[1]
        readings = [Reading(l) for l in lines[2:] if ':' in l]
        self.readings = {r.name: r for r in readings}

    def __getitem__(self, key):
        return self.readings[key]

    def __iter__(self):
        return iter(self.readings)

    def values(self):
        return self.readings.values()


@cached_value(3)
def _get_sensors_output():
    return subprocess.check_output(["sensors"]).decode()


def sensor_reading(pl, format, sensor, reading, threshold_good, threshold_bad):
    output = _get_sensors_output()
    sensors = {s.name: s for s in [Sensor(t) for t in output.split("\n\n") if t]}
    sensor = sensors[sensor][reading]
    gl = 100
    if sensor.value > 0:
        gl = calc_gradient(sensor.value, threshold_good, threshold_bad)
    return [{
        'contents': format.format(int(sensor.value)),
        'gradient_level': gl,
        'highlight_groups': ['cpu_load_percent_gradient', 'cpu_load_percent'],
    }]


def find_reading(name, sensor_name=None):
    output = _get_sensors_output()
    sensors = {s.name: s for s in [Sensor(t) for t in output.split("\n\n") if t]}
    if sensor_name:
        sensors = {sensor_name: sensors[sensor_name]}
    for sensor in sensors.values():
        for reading in sensor.values():
            if reading.name == name:
                return reading
    raise Exception("No reading '{}' found in: {}".format(name, output))


def read_fan_speed(pl, fan_number, threshold_good, threshold_bad):
    name = "fan{}".format(fan_number)
    sensor = find_reading(name)
    gl = 100
    if sensor.value > 0:
        gl = calc_gradient(sensor.value, threshold_good, threshold_bad)
    return [{
        'contents': "{} ⌘".format(int(sensor.value)),
        'gradient_level': gl,
        'highlight_groups': ['cpu_load_percent_gradient', 'cpu_load_percent'],
    }]


def read_cpu_temp(pl):
    sensor = find_reading("Tdie")
    gl = 100
    if sensor.value > 0:
        gl = calc_gradient(sensor.value, 35, 65)
    return [{
        'contents': "{}°C".format(int(sensor.value)),
        'gradient_level': gl,
        'highlight_groups': ['cpu_load_percent_gradient', 'cpu_load_percent'],
    }]
