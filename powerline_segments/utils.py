def calc_gradient(value, low, high):
    _range = high - low
    return min(100, max(0, value - low) * 100 / _range)
