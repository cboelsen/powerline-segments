from datetime import datetime, timedelta
_caches = {}

def cached_value(lifetime):
    assert lifetime > 0.5
    def _cached_value(fn):
        def _call_cached(*args, **kwargs):
            cached_result = _caches.get(fn)
            if cached_result:
                result, expiry = cached_result
                if datetime.now() < expiry:
                    return result
            result = fn(*args, **kwargs)
            _caches[fn] = (result, datetime.now() + timedelta(seconds=(lifetime - 0.5)))
            return result
        return _call_cached
    return _cached_value
