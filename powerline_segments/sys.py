import os
import pathlib
import subprocess

from powerline.segments import Segment, with_docstring

from .cache import cached_value
from .utils import calc_gradient


@cached_value(3)
def memory_usage(pl, format='{0:.1f}', interval=1,
                 threshold_good=30, threshold_bad=90):
    with open("/proc/meminfo") as f:
        output = f.readlines()
    total = int(output[0].split()[1])
    avail = int(output[2].split()[1])
    used = total - avail
    mem_pct = used * 100.0 / total
    return [{
        'contents': format.format(mem_pct),
        'gradient_level': calc_gradient(mem_pct, threshold_good, threshold_bad),
        'highlight_groups': ['cpu_load_percent_gradient', 'cpu_load_percent'],
    }]


@cached_value(30)
def apc_ups_power_usage(pl, threshold_good=30, threshold_bad=130):
    output = subprocess.check_output(['apcaccess'])
    for line in output.splitlines():
        if b'LOADPCT' in line:
            load_pct = float(line.strip().split(b' : ')[1].split()[0])
        elif b'NOMPOWER' in line:
            nom_power = int(line.strip().split(b' : ')[1].split()[0])
    power_usage = int(nom_power * load_pct / 100)
    return [{
        'contents': f"UPS {power_usage:3} W",
        'gradient_level': calc_gradient(power_usage, threshold_good, threshold_bad),
        'highlight_groups': ['cpu_load_percent_gradient', 'cpu_load_percent'],
    }]


def _render_number(number, letter):
    if number == 0:
        return "   "
    else:
        return f"{number:2}{letter}"


class MailUsage(Segment):
    interval = 20

    def __call__(self, pl, format="{0:2}n {1:2}u"):
        maildir = pathlib.Path(os.environ["HOME"]) / ".maildir"
        unread_mail = len([m for m in (maildir / "cur").iterdir() if "S" not in m.name.split(":")[1]])
        new_mail = len([m for m in (maildir / "new").iterdir()])
        gradient_level = 100 if new_mail > 0 else (50 if unread_mail > 0 else 0)

        return [{
            'contents': format.format(new_mail, unread_mail),
            'gradient_level': gradient_level,
            'highlight_groups': ['weather_temp_gradient', 'cpu_load_percent'],
        }]

mail_usage = cached_value(20)(with_docstring(
    MailUsage(),
    ""
))
