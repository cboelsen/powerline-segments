from setuptools import setup

setup(
    name='powerline-segments',
    version='0.1',
    description='Extra segments for powerline',
    url='https://gitlab.com/cboelsen/powerline-segments',
    license='MIT',
    packages=['powerline_segments'],
)
